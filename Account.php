<?php
session_start();
// si l'internaute accède à cette page sans être l'admin connecté alors
// on le renvoie vers la page indexphp
if (!isset($_SESSION['AdminConnecte']) AND !isset($_SESSION['UserConnecte'])) {
    header('location: index.php');
    die();
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="./include/styles.css"/>
    <title>Mon site !</title>
</head>
<body>
<?php
include("./include/header.php");
include("./include/connect.inc.php");
?>
<div class="wrapper">
    <?php include("./include/menus.php"); ?>
    <section id="content">
        <?php
        /********************
         * Account.php
         *********************/

        // le formulaire de saisie de la création d'une news
        echo "<h1>Créer un compte</h1>";
        echo "<BR/><BR/>";
        echo "<form method='post'>";

        $reqType = $conn->prepare("SELECT * FROM users WHERE id = ".$_SESSION['UserId']);
        $reqType->execute();

        foreach($reqType as $type) {
            echo "<label for='mail'>Addresse mail</label>";
            echo "<input type='email' name='mail' value='".$type['mail']."'/>";


            echo "<BR/><BR/>";

            echo "<label for='user'>Nom</label>";
            echo "<input type='text' name='user' value='".$type['name']."'/>";

            echo "<BR/><BR/>";

            echo "<label for='pass'>Mot de passe</label>";
            echo "<input type='password' name='pass' value='".$type['password']."'/>";

            echo "<BR/><BR/>";

        }




        echo "<input type='submit' name='Modifier' value='Modifier mes informations'/>";

        echo "</form>";

        // le formulaire a été soumis
        if (isset($_POST['Modifier']) && $_POST['Modifier'] != ""
            && isset($_POST['mail']) && $_POST['mail'] != ""
            && isset($_POST['user']) && $_POST['user'] != ""
            && isset($_POST['pass']) && $_POST['pass'] != ""
        ) {
            $reqEmpl = $conn->prepare("UPDATE users SET name = :user, mail = :mail, password = :pass WHERE id = :id");
            $reqEmpl->execute([
                ':mail' => $_POST['mail'],
                ':user' => $_POST['user'],
                ':pass' => $_POST['pass'],
                ':id' => $_SESSION["UserId"]
            ]);

            echo "<BR/><BR/>";
            echo "<p>Mise à jour du compte effectué !</p>";

            // on redirige vers index.php, il ne faut aucun affichage HTML (même <HEAD>...) avant une redirection
            header('location:index.php');
            // on arrêt l'execution pour ne pas executer les instructions plus bas
            die();
        } else if (isset($_POST['Ajouter'])) {
            echo "<p style='background: red; padding: 20px; color: white; font-size: 25px; font-weight: 900'>Remplir tous les champs</p>";
        }

        ?>
    </section>
</div>
<?php include("./include/footer.php"); ?>
</body>
</html>