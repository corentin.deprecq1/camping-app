<?php
session_start();
// si l'internaute accède à cette page sans être l'admin connecté alors
// on le renvoie vers la page indexphp


?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="./include/styles.css"/>
    <title>Mon site !</title>
</head>
<body>
<?php
include("./include/header.php");
include("./include/connect.inc.php");
?>
<div class="wrapper">
    <?php include("./include/menus.php"); ?>
    <section id="content">
        <?php

        $errorSaisie = false;
        $step = 0;

        if (!isset($_SESSION['UserConnecte'])) {
            echo "<h1>Oups...</h1>";
            echo "<h3>Pour effectuer une réservation vous devez être connecté en tant qu'utilisateur</h3>";
            echo "<a href='Connexion.php'>Se connecter </a>";
        }else{

            if (isset($_POST['Reserver'])) {
                $step = 1;
                $date1 =  strtotime($_POST['datedeb']);
                $date2 = strtotime($_POST['datefin']);

                $reqType = $conn->prepare("SELECT * FROM emplacement WHERE idEmpl = ".$_POST["emplId"]);
                $reqType->execute();
        
                $datemin;
                $datemax;
                foreach($reqType as $empl) {
                    $datemin = $empl["dateDebut"];
                    $datemax = $empl["dateFin"];
                }

                if(date("Y-m-d", $date1) >= date($datemin) && date("Y-m-d", $date2) <= date($datemax))
                {
                    $errorSaisie = false;

                }else{
                    $errorSaisie = "Les dates sélectionnées sont hors des périodes disponibles sur cet emplacement";
                }

                if($errorSaisie == false){
                    $total = 0;
                    echo "<h1>Confirmer ma réservation</h1>";
                    echo "<BR/><BR/>";
                    echo "<form method='post'>";
                    echo "<h4>Récapitulatif de ma réservation</h4>";
        
    
                    $nbJoursTimestamp = $date2 - $date1;
                    $nbJours = $nbJoursTimestamp/86400; 
                    echo "<BR/><BR/>";
                    
                    echo "Nombre de jours : ".$nbJours;
    
                    $date = getDate($date1);
    
                    if( in_array( $date["month"],["January","February","March", "April"])){
                        $time = " [moyenne saison]";
                        $req = $conn->prepare("SELECT prixMoyenneSaison AS price FROM emplacement WHERE idEmpl = ".$_POST["emplId"]);
                    }elseif(in_array( $date["month"],["May","June", "July","August"])){
                        $time = " [haute saison]";
                        $req = $conn->prepare("SELECT prixHauteSaison AS price FROM emplacement WHERE idEmpl = ".$_POST["emplId"]);
                    }else{
                        $time = " [basse saison]";
                        $req = $conn->prepare("SELECT prixBasseSaison AS price FROM emplacement WHERE idEmpl = ".$_POST["emplId"]);
                    }
                    echo "<BR/><BR/>";
    
                    $req->execute();
                    foreach($req as $empl) {
                        echo $time." - (".$empl['price']."€/j)";
                        echo " : ".$empl['price'] * $nbJours . "€";
    
                        $total += $empl['price'] * $nbJours;
                    }
        
                    echo "<BR/><BR/>";
    
                    $optionsPrice = [
                        "Barbecue"=>20,
                        "Wifi"=>5,
                        "Television"=>30,
                        "Climatisation"=>50,
    
                    ];
                    echo "Options sélectionnées : ";
                    echo "<BR/>";
    
                    $stringOpt = "";
                    
                    if(isset($_POST["options"] )){
                        foreach($_POST["options"] as $optName) {
                            echo "[".$optName."] (".$optionsPrice[$optName]."€/j)";
                            echo " : ".$optionsPrice[$optName] * $nbJours . "€";
                            $stringOpt.="$optName;";
                            echo "<BR/>";
                            $total += $optionsPrice[$optName] * $nbJours;
        
                        }
                    }
    
                    echo "Total : ". $total."€";
    
                    echo "<BR/>";
                    echo "<BR/>";
    
                    echo "<input type='hidden' name='idEmpl' value='".$_POST["emplId"]."'/>";
                    echo "<input type='hidden' name='datedeb' value='".$_POST['datedeb']."'/>";
                    echo "<input type='hidden' name='datefin' value='".$_POST['datefin']."'/>";
                    echo "<input type='hidden' name='options' value='".$stringOpt."'/>";
    
    
                    echo "<input type='submit' name='ConfirmReserver' value='Confirmer'/>";
                    echo "<a href='AddReservation.php?idEmpl=".$_POST["emplId"]."&datedeb=".$_POST['datedeb']."&datefin=".$_POST['datefin']."&options=".$stringOpt."'>Modifier</a>";
        
                    echo "</form>";
                }
                
            }
            
            
            if(isset($_POST['ConfirmReserver'])){
                $step = 2;
                echo "<h1>Réservation confirmée !</h1>";

                    $reqEmpl = $conn->prepare("INSERT INTO reservation (datedeb, datefin, idEmplacement, options, idUser) VALUES (:datedeb, :datefin, :idEmplacement, :options, :idUser)");
                    $reqEmpl->execute([
                        ':datedeb' => $_POST['datedeb'],
                        ':datefin' => $_POST['datefin'],
                        ':idEmplacement' => $_POST['idEmpl'],
                        ':options' => $_POST['options'],
                        ':idUser' => $_SESSION['UserId']
                        ]);

                    echo "<BR/><BR/>";
                    echo "<p>Ajout effectué !</p>";
                    echo "<a href='VoirLocations.php'>Voir mes réservations</a>";

                
            }
            
            
            if($errorSaisie || $step == 0){
                echo "<h1>Faire une réservation</h1>";
                echo "<BR/><BR/>";
                echo "<form method='post'>";
    
                echo "<label for='emplId'>Emplacement concerné : </label>";
                echo "<select name='emplId'>";
    
                $reqEmpl = $conn->prepare("SELECT emplacement.*, type.nomType FROM emplacement INNER JOIN type ON Type.idType = emplacement.idType");
                $reqEmpl->execute();
                echo "<option value='-1'>Veuillez sélectionner un emplacement</option>";
                foreach($reqEmpl as $empl) {
                    if(isset($_GET['idEmpl']) && $_GET['idEmpl'] == $empl['idEmpl']){
                        echo "<option value='".$empl['idEmpl']."' selected>[".$empl['nomType']."] ".$empl['adresseEmpl']."</option>";
    
                    }else{
                        echo "<option value='".$empl['idEmpl']."'>[".$empl['nomType']."] ".$empl['adresseEmpl']."</option>";
                    }
                }
                echo "</select>";
    
                echo "<BR/><BR/>";
    
                echo "<label for='datedeb'>Date de début du séjour</label>";
                if(isset($_GET['datedeb'])){
                    echo "<input type='date' name='datedeb' value='".$_GET['datedeb']."'/>";
                }else{
                    echo "<input type='date' name='datedeb'/>";
                }
    
                echo "<BR/><BR/>";
    
                echo "<label for='datefin'>Date de fin du séjour</label>";
                if(isset($_GET['datefin'])){
                    echo "<input type='date' name='datefin' value='".$_GET['datefin']."'/>";
                }else{
                    echo "<input type='date' name='datefin'/>";
                }
    
                echo "<p style='color:red'>".$errorSaisie."</p>";

                echo "<BR/><BR/>";
    
                echo "<label for='type'>Options : </label>";
                echo "<BR/>";
                if (isset($_GET['options']) && strpos($_GET['options'], 'Barbecue') !== false) {
                    echo '<input type="checkbox" id="bbq" name="options[]" value="Barbecue" checked>';
                }else{
                    echo '<input type="checkbox" id="bbq" name="options[]" value="Barbecue">';
                }
                echo '<label for="bbq"> Barbecue (20€/j)</label><br>';

                if (isset($_GET['options']) && strpos($_GET['options'], 'Wifi') !== false) {
                    echo '<input type="checkbox" id="wf" name="options[]" value="Wifi" checked>';
                }else{
                    echo '<input type="checkbox" id="wf" name="options[]" value="Wifi">';
                }
                echo '<label for="wf"> Wifi (5€/j)</label><br>';

                if (isset($_GET['options']) && strpos($_GET['options'], 'Television') !==false) {
                    echo '<input type="checkbox" id="tv" name="options[]" value="Television" checked>';
                }else{
                    echo '<input type="checkbox" id="tv" name="options[]" value="Television">';
                }
                echo '<label for="tv"> Télévision (30€/j)</label><br>';
                
                if (isset($_GET['options']) && strpos($_GET['options'], 'Climatisation') !== false) {
                    echo '<input type="checkbox" id="clim" name="options[]" value="Climatisation" checked>';
                }else{
                    echo '<input type="checkbox" id="clim" name="options[]" value="Climatisation">';
                }
                echo '<label for="clim"> Climatisation (50€/j)</label><br>';
             
                echo "<BR/><BR/>";
    
                echo "<input type='submit' name='Reserver' value='Réserver'/>";
    
                echo "</form>";
            }
        }
        ?>
    </section>
</div>
<?php include("./include/footer.php"); ?>
</body>
</html>