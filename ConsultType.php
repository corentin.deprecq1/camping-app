<?php
	session_start();

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<link rel="stylesheet" href="./include/styles.css" />
	<title>Mon site !</title>
</head>
<body>
	<?php 
		include("./include/header.php"); 
		include ("./include/connect.inc.php");
	?>
	<div class="wrapper">
		<?php include("./include/menus.php"); ?>
		<section id="content">
		<?php
			/********************
				ConsultType.php	
			*********************/
				// le formulaire de saisie du type d'emplacement recherché
				echo "<h1>Consulter les emplacements par type </h1>";
				echo "<BR/><BR/>";
				echo "<form method='post'>";
					echo "<fieldset>";
						echo "<legend> Types d'Emplacement </legend><BR/>";	
						// LD_Types = Liste Déroulante des types
						echo "<select name='LD_Types'>";
						// on constitue la liste déroulante à partir de la table Type 
						$reqType = $conn->prepare("SELECT * FROM Type");
						$reqType->execute();	
						foreach($reqType as $type) {
                            $isSelected = "";

                            if(isset($_POST['LD_Types']) && $_POST['LD_Types'] == $type['idType']){
                                $isSelected = "selected";
                            }
							echo "<option value='".$type["idType"]."' ".$isSelected.">".$type["nomType"]."</option>";
						}	
						$reqType->closeCursor();		
						echo "</select><br/><br/>";

                        $date = "";
                        if(isset($_POST['date'])){
                            $date = $_POST['date'];
                        }

                        echo "<label for='date' >Date de sejour</label>";
                        echo "<input type='date' value='".$date."' name='date'/>";

                        echo "<BR/><BR/>";

                        $nb = "";
                        if(isset($_POST['nbMax'])){
                            $nb = $_POST['nbMax'];
                        }

                        echo "<label for='nbMax'>Nb Occupants</label>";
                        echo "<input type='number' value='".$nb."' name='nbMax'/>";

                        echo "<BR/><BR/>";

						echo "<input type='submit' name='Afficher' value='Afficher'/>";
						echo "<br/><br/>";
					echo "</fieldset>";
				echo "</form>";		
				
				// le formulaire a été soumis
				if(isset($_POST['Afficher'])
                    && isset($_POST['LD_Types'])
                ) {
					// on affiche le tableau des résultats
					echo "<BR/><BR/>";
					echo "<center><table border='2' >";
						echo "<caption> Emplacement du Type ".$_POST['LD_Types']."</caption>";
						echo "<tr>
                                <th>Id Emp</th>
                                <th>Type de l'Emplacement</th>
                                <th>Adresse Emplacement</th>
                                <th>Année de Construction</th>
                                <th>Nb max Occupant</th>
                                <th>Date Début</th>
                                <th>Date Fin</th>
								<th>Photo</th>
								<th>Action</th>
                              </tr>";

                    $req = "SELECT * FROM Emplacement where";
                    $arr = array();

                    if(isset($_POST['LD_Types']) && $_POST['LD_Types'] != "") {
                        $req .= " idType = :pIdType ";
                        $arr["pIdType"] = $_POST['LD_Types'];
                    }
                    if(isset($_POST['nbMax'])&& $_POST['nbMax'] != "") {
                        $req .= " AND nbOccup >= :nbOccup ";
                        $arr["nbOccup"] = $_POST['nbMax'];
                    }
                    if(isset($_POST['date'])&& $_POST['date'] != "") {
                        $req .= " AND dateDebut <= :date AND dateFin >= :date ";
                        $arr["date"] = $_POST['date'];
                    }

                    $reqEmpl = $conn->prepare($req);
                    $reqEmpl->execute($arr);

                        foreach($reqEmpl as $empl) {
							echo "<tr>";
                            echo "<td>". "<a href='./DetailEmplacement.php?id=". $empl['idEmpl'] . "'>" . $empl['idEmpl'] . "</a></td>";
								echo "<td>".$empl["idType"]."</td>";
								echo "<td>".$empl["adresseEmpl"]."</td>";
								echo "<td>".$empl["anneeConstruction"]."</td>";
								echo "<td>".$empl["nbOccup"]."</td>";
								echo "<td>".$empl["dateDebut"]."</td>";
								echo "<td>".$empl["dateFin"]."</td>";
								echo "<td><a href='DetailEmplacement.php?id=". $empl['idEmpl'] . "'><img src='".$empl['imgPath']."'></a></td>";
								echo "<td><a href='AddReservation.php?idEmpl=".$empl['idEmpl']."&datedeb=".$empl["dateDebut"]."&datefin=".$empl["dateFin"]."'>Réserver</a></td>";

							echo "</tr>";
						}	
						$reqEmpl->closeCursor();
					echo "</table></center>";	
					echo "<BR/><BR/>";
				}		
		?>
		</section>
	</div>
	<?php include("./include/footer.php"); ?>
</body>
</html>