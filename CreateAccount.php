<?php
session_start();
// si l'internaute accède à cette page sans être l'admin connecté alors
// on le renvoie vers la page indexphp
//if (!isset($_SESSION['AdminConnecte'])) {
//    header('location: index.php');
//    die();
//}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="./include/styles.css"/>
    <title>Mon site !</title>
</head>
<body>
<?php
include("./include/header.php");
include("./include/connect.inc.php");
?>
<div class="wrapper">
    <?php include("./include/menus.php"); ?>
    <section id="content">
        <?php
        /********************
         * CreateNews.php
         *********************/
        // le formulaire de saisie de la création d'une news
        echo "<h1>Créer un compte</h1>";
        echo "<BR/><BR/>";
        echo "<form method='post'>";

        echo "<label for='mail'>Addresse mail</label>";
        echo "<input type='email' name='mail'/>";

        echo "<BR/><BR/>";

        echo "<label for='user'>Nom</label>";
        echo "<input type='text' name='user'/>";

        echo "<BR/><BR/>";

        echo "<label for='pass'>Mot de passe</label>";
        echo "<input type='password' name='pass'/>";

        echo "<BR/><BR/>";

        echo "<input type='submit' name='Creer' value='Créer mon compte'/>";

        echo "</form>";

        // le formulaire a été soumis
        if (isset($_POST['Creer']) && $_POST['Creer'] != ""
            && isset($_POST['mail']) && $_POST['mail'] != ""
            && isset($_POST['user']) && $_POST['user'] != ""
            && isset($_POST['pass']) && $_POST['pass'] != ""
        ) {
            $reqEmpl = $conn->prepare("INSERT INTO users (mail, name, password) VALUES (:mail, :user, :pass)");
            $reqEmpl->execute([
                ':mail' => $_POST['mail'],
                ':user' => $_POST['user'],
                ':pass' => $_POST['pass']
            ]);

            echo "<BR/><BR/>";
            echo "<p>Création du compte effectué !</p>";
        } else if (isset($_POST['Ajouter'])) {
            echo "<p style='background: red; padding: 20px; color: white; font-size: 25px; font-weight: 900'>Remplir tous les champs</p>";
        }

        ?>
    </section>
</div>
<?php include("./include/footer.php"); ?>
</body>
</html>