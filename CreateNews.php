<?php
session_start();
// si l'internaute accède à cette page sans être l'admin connecté alors
// on le renvoie vers la page indexphp
if (!isset($_SESSION['AdminConnecte'])) {
    header('location: index.php');
    die();
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="./include/styles.css"/>
    <title>Mon site !</title>
</head>
<body>
<?php
include("./include/header.php");
include("./include/connect.inc.php");
?>
<div class="wrapper">
    <?php include("./include/menus.php"); ?>
    <section id="content">
        <?php
        /********************
         * CreateNews.php
         *********************/
        // le formulaire de saisie de la création d'une news
        echo "<h1>Créer une news</h1>";
        echo "<BR/><BR/>";
        echo "<form method='post'>";

        echo "<label for='titre'>Titre de la news</label>";
        echo "<input type='text' name='titre'/>";

        echo "<BR/><BR/>";

        echo "<label for='resume'>Résumé</label>";
        echo "<input type='text' name='resume'/>";

        echo "<BR/><BR/>";

        echo "<label for='date'>Date</label>";
        echo "<input type='date' name='date'/>";

        echo "<BR/><BR/>";

        echo "<label for='type'>Type</label>";
        echo "
                <select name='type'>
                    <option value='important'>Important</option>
                    <option value='information'>Information</option>
                    <option value='annonce'>Annonce</option>
                    <option value='promotion'>Promotion</option>
                </select>";


        echo "<BR/><BR/>";

        echo "<label for='content'>Contenu de la news</label>";
        echo "<textarea name='content' cols='40' rows='5'></textarea>";

        echo "<BR/><BR/>";

        echo "<input type='submit' name='Ajouter' value='Ajouter'/>";

        echo "</form>";

        // le formulaire a été soumis
        if (isset($_POST['Ajouter']) && $_POST['Ajouter'] != ""
            && isset($_POST['content']) && $_POST['content'] != ""
            && isset($_POST['titre']) && $_POST['titre'] != ""
            && isset($_POST['type']) && $_POST['type'] != ""
            && isset($_POST['date']) && $_POST['date'] != ""
            && isset($_POST['resume']) && $_POST['resume'] != ""
        ) {
            $reqEmpl = $conn->prepare("INSERT INTO News (type, resume, datePub, nom, content) VALUES (:type, :resume, :date, :titre, :content)");
            $reqEmpl->execute([
                ':type' => $_POST['type'],
                ':content' => $_POST['content'],
                ':date' => $_POST['date'],
                ':titre' => $_POST['titre'],
                ':resume' => $_POST['resume']
                ]);

            echo "<BR/><BR/>";
            echo "<p>Ajout effectué !</p>";
        } else if(isset($_POST['Ajouter'])) {

            echo "<p style='background: red; padding: 20px; color: white; font-size: 25px; font-weight: 900'>Remplir tous les champs</p>";
        }

        ?>
    </section>
</div>
<?php include("./include/footer.php"); ?>
</body>
</html>