<?php
	session_start();
	unset($_SESSION['AdminConnecte']);
	unset($_SESSION['UserConnecte']);
	// on aurait pu faire aussi : 	session_destroy();
	// on redirige vers la page index.php
	header('location:index.php');
?>
