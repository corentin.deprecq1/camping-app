<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="./include/styles.css"/>
    <title>Mon site !</title>
</head>
<body>
<?php
session_start();
include("./include/header.php");
include("./include/connect.inc.php");

?>
<div class="wrapper">
    <?php include("./include/menus.php"); ?>
    <section id="content">

        <h1>Informations sur la location : </h1>
        <?php

        $id = $_GET["id"];

        $requete = "SELECT * FROM Emplacement WHERE idEmpl =" . $id;

        $reqEmpl = $conn->prepare($requete);
        $reqEmpl->execute();
        // on affiche le tableau des résultats
        echo "<BR/><BR/>";

        echo "<center><table border='2' >";
        //            echo "<tr><th>Id Empl</th><th>Type de l'emplacement</th><th>Adresse de l'emplacement</th><th>Année de construction</th></tr>";
        // affichage lignes du tableau
        foreach ($reqEmpl as $empl) {
            ?>
            <?php echo "<tr><td><img src='" . $empl['imgPath'] . "'></td></tr>"; ?>

            <tr>
                <td>ID de l'emplacement</td>
                <td><?php echo $empl['idEmpl'] ?></td>
            </tr>
            <tr>
                <td>Type d'emplacement</td>
                <td><?php echo $empl['idType'] ?></td>
            </tr>
            <tr>
                <td>Année de construction</td>
                <td><?php echo $empl['anneeConstruction'] ?></td>
            </tr>
            <tr>
                <td>adresse l'emplacement</td>
                <td><?php echo $empl['adresseEmpl'] ?></td>
            </tr>
            <tr>
                <td>Prix haute saison</td>
                <td><?php echo $empl['prixHauteSaison'] ?></td>
            </tr>
            <tr>
                <td>Prix moyenne saison</td>
                <td><?php echo $empl['prixMoyenneSaison'] ?></td>
            </tr>
            <tr>
                <td>Prix basse saison</td>
                <td><?php echo $empl['prixBasseSaison'] ?></td>
            </tr>


            <?php
        }
        $reqEmpl->closeCursor();
        echo "</table></center>";
        echo "<BR/><BR/>";

        $reqType = $conn->prepare("SELECT content, u.name, dateP FROM `avis` AS a JOIN users AS u ON a.idUser = u.id WHERE idEmpl = '".$id."' ORDER BY dateP");
        $reqType->execute();

        echo "<h1>Listes des commentaires :</h1>";
        foreach ($reqType as $type) {
            ?>
            <div class="comment">
                <h2><?php echo $type["name"]; ?></h2>
                <p><?php echo $type["dateP"]; ?></p>
                <p><?php echo $type["content"]; ?></p>
            </div>
            <?php

        }

        ?>
    </section>
</div>
<?php include("./include/footer.php"); ?>
</body>
</html>