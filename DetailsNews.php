<?php
session_start();
// Récupération des informations relatives à la news sélectionnée
if (isset($_GET['idNews'])){
    include ("./include/connect.inc.php");
    $requete = "SELECT * FROM News WHERE idNews = :idNews;";
    $reqEmpl = $conn->prepare($requete);
    $reqEmpl->execute([":idNews"=>$_GET['idNews']]);
    $news = $reqEmpl-> fetch();
}else{
    header('location: index.php');
    die();
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="./include/styles.css"/>

    <title>Mon site !</title>
</head>
<body>
<?php
include("./include/header.php");
include("./include/connect.inc.php");
?>
<div class="wrapper">
    <?php include("./include/menus.php"); ?>
    <section id="content">
        <?php
        // le formulaire de saisie du type d'emplacement recherché
        echo "<h1>Titre : ".$news["nom"]."</h1>";
        echo "<h3>Résumé : ".$news["resume"]."</h3>";
        echo "<span class='badge'>".$news["datePub"]."</span>";
        echo "<span class='badge bg-secondary'>".$news["type"]."</span>";
        echo "<p>".$news["content"]."</p>";
        ?>
    </section>
</div>
<?php include("./include/footer.php"); ?>
</body>
</html>