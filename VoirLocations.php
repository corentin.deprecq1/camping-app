<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="./include/styles.css"/>
    <title>Mon site !</title>
</head>
<body>
<?php
session_start();
include("./include/header.php");
include("./include/connect.inc.php");

?>
<div class="wrapper">
    <?php include("./include/menus.php"); ?>
    <section id="content">

        <h1>Mes locations : </h1>
        <?php

        $userId = $_GET["userId"];

        if (isset($_GET["idSuppression"])) {
            // Update en base du champ qui montre qu'il faut supprimer cette réservation

            $deleteRequete = "UPDATE reservation SET attenteSuppression = TRUE WHERE id =" . $_GET["idSuppression"];

            $reqSuppression = $conn->prepare($deleteRequete);
            $reqSuppression->execute();

            // Redirection sur la page courante
        }

        //on cherche toutes les locations d'un client donné grâce à son ID passé en paramètre


        $requete = "SELECT * FROM reservation WHERE idUser =" . $userId;

        $reqEmpl = $conn->prepare($requete);
        $reqEmpl->execute();

        // affichage lignes du tableau
        foreach ($reqEmpl as $empl) {
            ?>
            <BR/><BR/>

            <?php
            $dateFin = strtotime($empl['dateFin']);
            $isFinished = false;
            if (strtotime(date('Y-m-d')) > $dateFin)
                echo "<p>Location terminée</p>";
            ?>
            <center>
                <table border='2'>

                    <tr>
                        <td>ID Emplacement</td>
                        <td>
                            <a href='./DetailEmplacement.php?id= <?php echo $empl['idEmplacement'] ?> '><?php echo $empl['idEmplacement'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Date de début</td>
                        <td><?php echo $empl['dateDeb'] ?></td>
                    </tr>
                    <tr>
                        <td>Date de fin</td>
                        <td><?php echo $empl['dateFin'] ?></td>
                    </tr>
                    <tr>
                        <td>Options</td>
                        <td><?php echo $empl['options'] ?></td>
                    </tr>
                    <tr>
                        <td>Prix total</td>
                        <td>TBD</td>
                    </tr>

                </table>
            </center>

            <?php if ($empl['attenteSuppression']) { ?>
                <p>En attente d'annulation</p>
            <?php } else { ?>
                <a href="VoirLocations.php?userId=<?php echo $empl['idUser'] ?>&idSuppression=<?php echo $empl['id'] ?>">Demande
                    d'annulation</a>
            <?php } ?>
            <br>
            <?php if (strtotime(date('Y-m-d')) < $dateFin) { ?>
                <!--TODO câbler le bouton de modification de sa réservation-->
                <a href="#">Consulter formulaire réservation</a>
            <?php } ?>
            <BR/><BR/>
            <?php
        }

        //on cherche toutes les locations d'un client donné grâce à son ID passé en paramètre


        $requete = "SELECT * FROM reservation WHERE idUser =" . $_SESSION["UserId"];

        $reqEmpl = $conn->prepare($requete);
        $reqEmpl->execute();

        // affichage lignes du tableau
        foreach ($reqEmpl as $empl) {
            ?>
            <BR/><BR/>

            <?php
            $date = strtotime($empl['dateFin']);

            if (strtotime(date('Y-m-d')) > $date)
                echo "<p>Location terminée</p>"
            ?>
            <center>
                <table border='2'>

                    <tr>
                        <td>ID Emplacement</td>
                        <td>
                            <a href='./DetailEmplacement.php?id=<?php echo $empl['idEmplacement'] ?> '><?php echo $empl['idEmplacement'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Date de début</td>
                        <td><?php echo $empl['dateDeb'] ?></td>
                    </tr>
                    <tr>
                        <td>Date de fin</td>
                        <td><?php echo $empl['dateFin'] ?></td>
                    </tr>
                    <tr>
                        <td>Options</td>
                        <td><?php echo $empl['options'] ?></td>
                    </tr>
                    <tr>
                        <td>Prix total</td>
                        <td>TBD</td>
                    </tr>

                </table>
            </center>
            <BR/><BR/>

            <?php
            if (date("Y-m-d", strtotime($empl['dateFin']))
                < date("Y-m-d")) { ?>
                <form method='post'>

                    <label for='comment'>Commentaire</label>
                    <textarea name='comment' cols='40' rows='5'></textarea>

                    <input type='submit' name='Ajouter' value='Ajouter un avis'/>

                </form>

            <?php } ?>

            <?php

            if (isset($_POST['Ajouter']) && $_POST['Ajouter'] != ""
                && isset($_POST['comment']) && $_POST['comment'] != "") {

                $reqEmpl = $conn->prepare("INSERT INTO Avis (idUser, idEmpl, content, dateP) VALUES (:idUser, :idEmpl, :comment, :dateP)");
                $reqEmpl->execute([
                    ':idUser' => $_SESSION['UserId'],
                    ':comment' => $_POST['comment'],
                    ':dateP' => date("Y-m-d"),
                    ':idEmpl' => $empl['idEmplacement']
                ]);

                echo "<p>Ajout de l'avis effectué !</p>";
            }
            ?>
            <?php
        }
        ?>
    </section>
</div>
<?php include("./include/footer.php"); ?>
</body>
</html>