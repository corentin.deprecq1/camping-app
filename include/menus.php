<nav class="sidebar">
  <ul>
    <li><a href="index.php">Accueil</a></li>
    <li><a href="ConsultDate.php">Consulter les emplacements par année de construction/rénovation</a></li>
    <li><a href="ConsultNews.php">Consulter les news par année de publication</a></li>
	<li><a href="ConsultType.php">Consulter les emplacements par type</a></li>
	<li><a href="AddReservation.php">Faire une réservation</a></li>

<?php
		// si l'admin ne s'est pas déja connecté alors on affiche le lien pour cela
		if (isset($_SESSION['AdminConnecte'])) {
            echo '<li><a href="CreateNews.php">Créer une News</a></li>';
            echo '<li><a href="managereservation.php">Annuler ou Accepter une réservation</a></li>';
            echo '<li><a href="Deconnexion.php">Se déconnecter </a></li>';

        } elseif (isset($_SESSION['UserConnecte'])) {
            echo '<li><a href="Account.php">Mon compte</a></li>';
            echo '<li><a href="VoirLocations.php?userId=' . $_SESSION['UserId']. '">Mes locations</a></li>';
            echo '<li><a href="Deconnexion.php">Se déconnecter </a></li>';
        }
		else {
            echo '<li><a href="CreateAccount.php">Créer un compte</a></li>';
            echo '<li><a href="Connexion.php">Se connecter </a></li>';
		}
?>
  </ul>
</nav>
