DROP TABLE IF EXISTS Emplacement;
DROP TABLE IF EXISTS Type;
DROP TABLE IF EXISTS News;


CREATE TABLE Type (
idType VARCHAR(4),
nomType VARCHAR(25),
PRIMARY KEY (idType)
) Engine=InnoDB;

CREATE TABLE Emplacement (
idEmpl VARCHAR(3),
idType VARCHAR(4) NOT NULL,
adresseEmpl VARCHAR(50),
anneeConstruction mediumint(4),
imgPath VARCHAR(50),
PRIMARY KEY (idEmpl),
FOREIGN KEY(idType) REFERENCES Type(idType)
)Engine=InnoDB; 


INSERT INTO Type (idType, nomType) VALUES ('100', 'Bungalow'); 
INSERT INTO Type (idType, nomType) VALUES ('200', 'Mobil-Home'); 
INSERT INTO Type (idType, nomType) VALUES ('300', 'Emplacement');

INSERT INTO Emplacement (idEmpl, idType, adresseEmpl, anneeConstruction, imgPath) VALUES ('115', '100','4 rue du Soleil',2012, "./images/empl115.jpg");
INSERT INTO Emplacement (idEmpl, idType, adresseEmpl, anneeConstruction, imgPath) VALUES ('198', '100', '7 rue des Fleurs', 2008, "./images/empl198.jpg");
INSERT INTO Emplacement (idEmpl, idType, adresseEmpl, anneeConstruction, imgPath) VALUES ('231', '200', '5 rue des Fleurs', 2003, "./images/empl231.jpg");
INSERT INTO Emplacement (idEmpl, idType, adresseEmpl, anneeConstruction, imgPath) VALUES ('302', '300', '1 rue de la Plage', 1999, "./images/empl302.jpg");
INSERT INTO Emplacement (idEmpl, idType, adresseEmpl, anneeConstruction, imgPath) VALUES ('357', '300', '12 rue des Pins', 2016, "./images/empl357.jpg");	

CREATE TABLE News (
    idNews INTEGER AUTO_INCREMENT,
    type VARCHAR(50),
    resume VARCHAR(50),
    datePub DATE,
    nom VARCHAR(50),
    content TEXT,
    PRIMARY KEY (idNews)
)Engine=InnoDB;

COMMIT;





