CREATE TABLE Avis
(
    idUser  INTEGER,
    idEmpl  VARCHAR(3),
    content VARCHAR(250),
    dateP   DATE,
    id      INTEGER AUTO_INCREMENT,
    PRIMARY KEY (id),
    FOREIGN KEY (idEmpl) REFERENCES Emplacement (idEmpl),
    FOREIGN KEY (idUser) REFERENCES Users (id)
) Engine=InnoDB;