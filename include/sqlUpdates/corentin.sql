INSERT INTO News VALUES (1, 'Inmportant', 'Je suis le resume d un article', '2023/02/08', 'La peste', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. At ea ex, fugit illo impedit ipsa ipsum iste laudantium minima nemo nobis, officia perspiciatis quia repellendus tempora, veniam voluptatum. Consectetur, illum!');

ALTER TABLE `Emplacement`
    ADD `nbOccup` INT NOT NULL AFTER `prixBasseSaison`,
    ADD `dateDebut` DATE NOT NULL AFTER `nbOccup`,
    ADD `dateFin` DATE NOT NULL AFTER `dateDebut`;

UPDATE Emplacement
SET dateDebut = '2023-05-15', nbOccup = 50, dateFin = '2024-05-15'
WHERE idEmpl = '115';

UPDATE Emplacement
SET dateDebut = '2006-05-15', nbOccup = 150, dateFin = '2009-05-15'
WHERE idEmpl = '198';


UPDATE Emplacement
SET dateDebut = '1995-05-15', nbOccup = 5, dateFin = '1998-05-15'
WHERE idEmpl = '231' OR idEmpl = '302' OR idEmpl = '357';
