ALTER TABLE `Emplacement`
    ADD `prixHauteSaison` INT NOT NULL AFTER `imgPath`,
    ADD `prixMoyenneSaison` INT NOT NULL AFTER `prixHauteSaison`,
    ADD `prixBasseSaison` INT NOT NULL AFTER `prixMoyenneSaison`;