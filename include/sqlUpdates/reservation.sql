CREATE TABLE reservation (
dateDeb DATE,
dateFin DATE,
options VARCHAR(50),
isAccepted tinyint(1) NOT NULL DEFAULT 0,
idEmplacement INTEGER,
idUser INTEGER,
id  INTEGER AUTO_INCREMENT,
PRIMARY KEY (id),
) Engine=InnoDB;


INSERT INTO reservation (dateDeb, dateFin, options, idEmplacement, idUser) VALUES ('2023/06/08', '2023/06/15', "Barbecue;Wifi;", 115, 1);