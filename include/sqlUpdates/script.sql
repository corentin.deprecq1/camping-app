DROP TABLE IF EXISTS Emplacement;
DROP TABLE IF EXISTS Type;
DROP TABLE IF EXISTS News;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS reservation;


CREATE TABLE Type (
                      idType VARCHAR(4),
                      nomType VARCHAR(25),
                      PRIMARY KEY (idType)
) Engine=InnoDB;

CREATE TABLE Emplacement (
                             idEmpl VARCHAR(3),
                             idType VARCHAR(4) NOT NULL,
                             adresseEmpl VARCHAR(50),
                             anneeConstruction mediumint(4),
                             imgPath VARCHAR(50),
                             PRIMARY KEY (idEmpl),
                             FOREIGN KEY(idType) REFERENCES Type(idType)
)Engine=InnoDB;


INSERT INTO Type (idType, nomType) VALUES ('100', 'Bungalow');
INSERT INTO Type (idType, nomType) VALUES ('200', 'Mobil-Home');
INSERT INTO Type (idType, nomType) VALUES ('300', 'Emplacement');

INSERT INTO Emplacement (idEmpl, idType, adresseEmpl, anneeConstruction, imgPath) VALUES ('115', '100','4 rue du Soleil',2012, "./images/empl115.jpg");
INSERT INTO Emplacement (idEmpl, idType, adresseEmpl, anneeConstruction, imgPath) VALUES ('198', '100', '7 rue des Fleurs', 2008, "./images/empl198.jpg");
INSERT INTO Emplacement (idEmpl, idType, adresseEmpl, anneeConstruction, imgPath) VALUES ('231', '200', '5 rue des Fleurs', 2003, "./images/empl231.jpg");
INSERT INTO Emplacement (idEmpl, idType, adresseEmpl, anneeConstruction, imgPath) VALUES ('302', '300', '1 rue de la Plage', 1999, "./images/empl302.jpg");
INSERT INTO Emplacement (idEmpl, idType, adresseEmpl, anneeConstruction, imgPath) VALUES ('357', '300', '12 rue des Pins', 2016, "./images/empl357.jpg");

CREATE TABLE reservation (
                             dateDeb DATE,
                             dateFin DATE,
                             options VARCHAR(50),
                             isAccepted tinyint(1) NOT NULL DEFAULT 0,
                             idEmplacement INTEGER,
                             idUser INTEGER,
                             id  INTEGER AUTO_INCREMENT,
                             PRIMARY KEY (id)
) Engine=InnoDB;


INSERT INTO reservation (dateDeb, dateFin, options, idEmplacement, idUser) VALUES ('2023/06/08', '2023/06/15', "Barbecue;Wifi;", 115, 1);

CREATE TABLE News (
                      idNews INTEGER AUTO_INCREMENT,
                      type VARCHAR(50),
                      resume VARCHAR(50),
                      datePub DATE,
                      nom VARCHAR(50),
                      content TEXT,
                      PRIMARY KEY (idNews)
)Engine=InnoDB;

ALTER TABLE `Emplacement`
    ADD `prixHauteSaison` INT NOT NULL AFTER `imgPath`,
    ADD `prixMoyenneSaison` INT NOT NULL AFTER `prixHauteSaison`,
    ADD `prixBasseSaison` INT NOT NULL AFTER `prixMoyenneSaison`;

INSERT INTO News VALUES (1, 'Inmportant', 'Je suis le resume d un article', '2023/02/08', 'La peste', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. At ea ex, fugit illo impedit ipsa ipsum iste laudantium minima nemo nobis, officia perspiciatis quia repellendus tempora, veniam voluptatum. Consectetur, illum!');

ALTER TABLE `Emplacement`
    ADD `nbOccup` INT NOT NULL AFTER `prixBasseSaison`,
    ADD `dateDebut` DATE AFTER `nbOccup`,
    ADD `dateFin` DATE AFTER `dateDebut`;

UPDATE Emplacement
SET dateDebut = '2023-05-15', nbOccup = 50, dateFin = '2024-05-15'
WHERE idEmpl = '115';

UPDATE Emplacement
SET dateDebut = '2006-05-15', nbOccup = 150, dateFin = '2009-05-15'
WHERE idEmpl = '198';


UPDATE Emplacement
SET dateDebut = '1995-05-15', nbOccup = 5, dateFin = '1998-05-15'
WHERE idEmpl = '231' OR idEmpl = '302' OR idEmpl = '357';

CREATE TABLE users (
                       name VARCHAR(50),
                       password VARCHAR(50),
                       mail VARCHAR(50),
                       id  INTEGER AUTO_INCREMENT,
                       PRIMARY KEY (id)
) Engine=InnoDB;


INSERT INTO users (name, password, mail) VALUES ('user', 'user!', "user@gmail.com");

CREATE TABLE Avis
(
    idUser  INTEGER,
    idEmpl  VARCHAR(3),
    content VARCHAR(250),
    dateP   DATE,
    id      INTEGER AUTO_INCREMENT,
    PRIMARY KEY (id),
    FOREIGN KEY (idEmpl) REFERENCES Emplacement (idEmpl),
    FOREIGN KEY (idUser) REFERENCES users (id)
) Engine=InnoDB;

ALTER TABLE `reservation` ADD `attenteSuppression` TINYINT NOT NULL AFTER `id`;

COMMIT;