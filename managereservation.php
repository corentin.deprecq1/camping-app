<?php
session_start();
// si l'internaute accède à cette page sans être l'admin connecté alors
// on le renvoie vers la page indexphp
if (!isset($_SESSION['AdminConnecte'])) {
    header('location: index.php');
    die();
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="./include/styles.css"/>
    <title>Mon site !</title>
</head>
<body>
<?php
include("./include/header.php");
include("./include/connect.inc.php");
?>
<div class="wrapper">
    <?php include("./include/menus.php"); ?>
    <section id="content">
        <?php
        if(isset($_GET['id'])){

            if(isset($_POST['refuser'])){
                $rq = "UPDATE reservation SET isAccepted = -1 WHERE id = ".$_GET['id'].";";
                $reqBool = $conn->prepare($rq);
                $reqBool -> execute();
            }else if(isset($_POST['valider'])){
                $rq = "UPDATE reservation SET isAccepted = 1 WHERE id = ".$_GET['id'].";";
                $reqBool = $conn->prepare($rq);
                $reqBool -> execute();
            }
        }
        $requete = "SELECT * FROM reservation WHERE isAccepted = 0";
        $reqEmpl = $conn->prepare($requete);
        $reqEmpl->execute();

        echo "<table border='1'>";
        echo "<thead>
        <tr>
            <th>Date de début</th>
            <th>Date de fin</th>
            <th>Options</th>
            <th>Emplacement N°</th>
            <th>N° Client</th>
        </tr>
    </thead><tbody>";
        foreach ($reqEmpl as $res) {
            echo "<tr>";
                echo "<td>".$res["dateDeb"]."</td>";
                echo "<td>".$res["dateFin"]."</td>";
                echo "<td>".$res["options"]."</td>";
                echo "<td>".$res["idEmplacement"]."</td>";
                echo "<td>".$res["idUser"]."</td>";
                echo "<form method='post' action='managereservation.php?id=".$res["id"]."'>";
                echo "<td><button name='refuser' id='refuser' type='submit' style='background-color: red; padding: 5px'>Refuser</button></td>";
                echo "<td><button name='valider' id='valider' type='submit' style='background-color: green; padding: 5px'>Valider</button></td>";
                echo "</form>";

            echo "</tr>";
        }

        echo "</tbody>";
        $reqEmpl -> closeCursor()
        ?>
    </section>
</div>

</body>
</html>